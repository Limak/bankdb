--wypelnienie PRACOWNICY
INSERT INTO PRACOWNICY VALUES('Jan', 'Kowalski', 'Analityk Kredytowy', '00291009454', 'Gdansk, ul.Walowa 22a/12','Gdansk, ul.Walowa 22a/12', '85885442548381899148395099', 'ok');
INSERT INTO PRACOWNICY VALUES('Lorena','Anderson', 'Analityk Kredytowy', '10252602460', 'Gdansk, ul.Zielona 30/1','Gdansk, ul.3Maja 12a/72', '60808123489003664891184375', 'moze byc lepiej');
INSERT INTO PRACOWNICY VALUES('Bryant', 'Malone', 'Analityk Kredytowy', '91110902205', 'Krakow, ul.Pomasa 21/1c','Gdansk, ul.Walowa 1e/76', '77795123127175520790827926', 'najlepszy pracownik');
INSERT INTO PRACOWNICY VALUES('Angelina', ' Rhodes', 'Analityk Kredytowy','91110902205', 'Krakow, ul.Pomasa 21/1c','Gdansk, ul.Walowa 1e/76', '77120696994044845418435182','musi wiecej nadgodzin robic');
INSERT INTO PRACOWNICY VALUES('Jeremy', 'Terry', 'Analityk Kredytowy','91110902205', 'Krakow, ul.Pomasa 21/1c', 'Gdansk, ul.Walowa 1e/76', '31446043273182157394741262', 'wszystko ok');
INSERT INTO PRACOWNICY VALUES('Bronimir', 'G�rniak', 'Analityk Kredytowy', '64030571545', 'Gdansk, dr. Stanis�awa','Gdansk, dr. Stanis�awa', '547278150685716309927629', 'ok');
INSERT INTO PRACOWNICY VALUES('Radzimierz', 'Ch�ci�ski', 'Analityk Kredytowy', '74041282295', 'Gdansk, ul.Z. Smodlibowskiej', 'Gdansk, ul.Z. Smodlibowskiej', '35311407247792141023206769', 'ok');
INSERT INTO PRACOWNICY VALUES('Anita', 'Borecka', 'Analityk Kredytowy', '82051172956', 'Gda�sk, ul.Ignacego Jerzego', 'Gdansk, ul.Ignacego Jerzego', '171780274147777364130780795651', 'ok');
INSERT INTO PRACOWNICY VALUES('Axel', 'Ba�a', 'Analityk Kredytowy', '98082858613', 'Gda�sk, ul.Ba�niowa','Gdansk, ul.Ba�niowa',  '63016663117944004843978283', 'ok');
INSERT INTO PRACOWNICY VALUES('Gall', 'Idziak', 'Analityk Kredytowy', '64080427575', 'Gda�sk, ul.Bzury','Gdansk, ul.Bzury', '91378073753442220236496171', 'ok');




--wypelnienie CZYNNOSCI
INSERT INTO CZYNNOSCI VALUES('rozparzenie wniosku', '12');
INSERT INTO CZYNNOSCI VALUES('pozytywne decyzje', '10');
INSERT INTO CZYNNOSCI VALUES('organizacja szkolen', '20');
INSERT INTO CZYNNOSCI VALUES ('delegacje', '15');
INSERT INTO CZYNNOSCI VALUES ('telefony na policje', '5');
INSERT INTO CZYNNOSCI VALUES ('nadgodziny', '4');
INSERT INTO CZYNNOSCI VALUES ('negatywne decyzje', '12');
INSERT INTO CZYNNOSCI VALUES ('wstrzymane decyzje', '10');
INSERT INTO CZYNNOSCI VALUES ('pomoc w weryfikacji', '12');
INSERT INTO CZYNNOSCI VALUES ('telefony do klientow', '2');


--wypelnienie ABSENCJE
INSERT INTO ABSENCJE VALUES ('2016-11-09T13:52:25', '2017-11-29 03:02:46', 'urlop macierzy�ski', '4');
INSERT INTO ABSENCJE VALUES ('2016-09-27T05:45:25', '2016-10-30T02:11:51', 'choroba', '1');
INSERT INTO ABSENCJE VALUES ('2016-08-26 04:51:45', '2017-02-08 18:08:27', 'urlop tatusiowy', '3');
INSERT INTO ABSENCJE VALUES ('2016-10-05 00:02:23', '2017-10-10 15:58:01', 'urlop zdrowotny', '10');
INSERT INTO ABSENCJE VALUES ('2017-08-18 17:53:34', '2017-11-06 19:44:41', 'delegacja', '2');
INSERT INTO ABSENCJE VALUES ('2017-01-02 01:16:18', '2017-02-21 17:31:09', 'L4', '5');
INSERT INTO ABSENCJE VALUES ('2017-02-22 22:41:06', '2017-02-25 21:57:38', 'L4', '7');
INSERT INTO ABSENCJE VALUES ('2017-03-05 23:37:50', '2017-03-11 02:10:25', 'delegacja', '5');
INSERT INTO ABSENCJE VALUES ('2017-03-27 17:41:10', '2017-04-22 07:07:10', 'delegacja', '1');
INSERT INTO ABSENCJE VALUES ('2017-05-16 18:42:19', '2017-05-17 05:47:37', 'L4', '10');


--wypelnienie AKTYWNOSCI
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-01-16 19:58:46', 2);
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-01-17 16:24:14', 1);
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-02-14 17:16:10', 4);
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-01-16 19:58:46', 5);
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-02-14 17:16:10', 1);
INSERT INTO AKTYWNOSCI VALUES ('2', '2017-02-14 15:16:10', 1);
INSERT INTO AKTYWNOSCI VALUES ('7', '2017-03-06 09:00:31', 10);
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-03-06 09:00:31', 3);
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-01-16 19:58:46', 7);
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-01-16 19:58:46', 1);
INSERT INTO AKTYWNOSCI VALUES ('1', '2017-01-16 19:58:46', 3);


--wypelnienei WYPLATY
INSERT INTO WYPLATY VALUES ('2000.00', '2017-01-16 19:58:46', '1');
INSERT INTO WYPLATY VALUES ('2500.00', '2017-01-16 19:58:46', '2');
INSERT INTO WYPLATY VALUES ('5000.00', '2017-01-16 19:58:46', '3');
INSERT INTO WYPLATY VALUES ('3000.00', '2017-01-16 19:58:46', '4');
INSERT INTO WYPLATY VALUES ('2000.00', '2017-01-16 19:58:46', '5');
INSERT INTO WYPLATY VALUES ('3500.00', '2017-01-16 19:58:46', '6');
INSERT INTO WYPLATY VALUES ('2500.00', '2017-01-16 19:58:46', '7');
INSERT INTO WYPLATY VALUES ('2000.00', '2017-01-16 19:58:46', '8');
INSERT INTO WYPLATY VALUES ('2000.00', '2017-01-16 19:58:46', '9');
INSERT INTO WYPLATY VALUES ('2000.00', '2017-01-16 19:58:46', '10');

INSERT INTO WYPLATY VALUES ('2500.00', '2017-02-16 19:58:46', '2');
INSERT INTO WYPLATY VALUES ('3000.00', '2017-03-16 19:58:46', '2');


--wypelnienie ZMIANY
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 18:00:00', '1');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:15:00', '2017-01-16 17:05:00', '2');
INSERT INTO ZMIANY VALUES ('2017-01-16 9:00:00', '2017-01-16 18:00:00', '3');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 15:05:00', '4');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 18:00:00', '5');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 18:00:00', '6');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 18:00:00', '7');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 18:00:00', '8');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 18:00:00', '9');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 18:00:00', '10');

INSERT INTO ZMIANY VALUES ('2017-03-16 8:00:00', '2017-03-16 17:06:00', '1');
INSERT INTO ZMIANY VALUES ('2017-01-16 8:00:00', '2017-01-16 18:30:00', '1');

--wypelnienie KLIENCI
INSERT INTO KLIENCI VALUES('1');	--1
INSERT INTO KLIENCI VALUES('2');	--2
INSERT INTO KLIENCI VALUES('3');	--3
INSERT INTO KLIENCI VALUES ('4');	--4
INSERT INTO KLIENCI VALUES ('5');	--5
INSERT INTO KLIENCI VALUES ('6');	--6
INSERT INTO KLIENCI VALUES ('7');	--7
INSERT INTO KLIENCI VALUES ('8');	--8
INSERT INTO KLIENCI VALUES ('9');	--9
INSERT INTO KLIENCI VALUES ('10');	--10
INSERT INTO KLIENCI VALUES ('11');
INSERT INTO KLIENCI VALUES ('12');
INSERT INTO KLIENCI VALUES ('13');
INSERT INTO KLIENCI VALUES ('14');
INSERT INTO KLIENCI VALUES ('15');
INSERT INTO KLIENCI VALUES ('16');
INSERT INTO KLIENCI VALUES ('17');
INSERT INTO KLIENCI VALUES ('18');
INSERT INTO KLIENCI VALUES ('19');
INSERT INTO KLIENCI VALUES ('20');

--wypelnienie KLIENCI_INDYWIDUALNI
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Sofroniusz', 'Kosmala', '55100861749', 'ul.Edmunda Markowskiego 12c/43', 'ul.Edmunda Markowskiego 12c/43', '1');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Debora', 'Majewski', '55011923149', 'ul.Sadurkowska 1c/43', 'ul.Sadurkowska 1c/3', '2');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Ksenia', 'Wachowiak', '52122505330', 'ul.Szymborskiej 4c/3', 'ul.Szymborskiej 4c/3', '3');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Ulrych', 'B�k', '64062937555', 'ul.Uko�na 30a/1', 'ul.Uko�na 30a/1', '4');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Benon', 'Wilczek', '69090633680', 'ul.1 PLM Warszawa 31/89', 'ul.1 PLM Warszawa 31/89', '5');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Ro�cis�awa', 'Babiarz', '57081926631', 'ul.Edmunda Cie�kiewicza 76f/3', 'ul.Edmunda Cie�kiewicza 76f/3', '6');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Sewera', 'Kasprowicz', '61112177558', 'ul.Lipka 45/2', 'ul.Lipka 45/2', '7');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Zbroimir', 'Kaszuba', '93061544657', 'ul.ks. Jerzego Badestinusa 15g/21', 'ul.ks. Jerzego Badestinusa 15g/21', '8');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Radogost', 'K�dzia', '54112826973', 'ul.Obro�c�w �askarzewa 12c/43', 'ul.Obro�c�w �askarzewa 12c/43', '9');
INSERT INTO KLIENCI_INDYWIDALNI VALUES ('Roman', 'Jura', '78022131402', 'ul.Heleny i Jana Prze�lak�w 1a/17', 'ul.Heleny i Jana Prze�lak�w 1a/17', '10');


--wypelnienie KLIENCI_FIRMY
INSERT INTO  KLIENCI_FIRMY VALUES ('Mega w�skie S�odycze s.c.', '3867130793', 'ul.IX Wiek�w Kielc', '11');
INSERT INTO  KLIENCI_FIRMY VALUES ('Super wygodne Korki s.c.', '5131444680','ul.�uczniczki', '12');
INSERT INTO  KLIENCI_FIRMY VALUES ('Wypasione  Sp. Z o.o.', '2362058113','ul.55 Pozna�skiego Pu�ku Piechoty', '13');
INSERT INTO  KLIENCI_FIRMY VALUES ('MegaliteSport.sk', '9141881361','ul.Idziego Biernackiego', '14');
INSERT INTO  KLIENCI_FIRMY VALUES ('SupermotaTriple.sk', '1224842230','ul.Leszka Raciborskiego', '15');
INSERT INTO  KLIENCI_FIRMY VALUES ('Super bycze Kurtki Sp. Z o.o.', '8896202115', 'Ireny Stablewskiej','16');
INSERT INTO  KLIENCI_FIRMY VALUES ('Mega witkie Alkohole Sp. Z o.o.', '8316182710','ul.Rotmistrza Witolda Pileckiego', '17');
INSERT INTO  KLIENCI_FIRMY VALUES ('Megarateryfe3D.pl', '7293530324', 'ul. Lio�ska', '18');
INSERT INTO  KLIENCI_FIRMY VALUES ('Ultra witkie Opony', '8649530600', 'ul.�owcza', '19');
INSERT INTO  KLIENCI_FIRMY VALUES ('Hiper rzutkie Korki Sp. Z o.o.', '2667286662','ul.Ukryta', '20');


--wypelnienie WNIOSKI
INSERT INTO WNIOSKI VALUES ('2017-01-16 19:58:46', 'pog��biona', 'pozytywny', 'hipoteczny', '50000.0', '10', '1', '10.00', 'tak', 'nie');
INSERT INTO WNIOSKI VALUES ('2017-01-17 16:24:14', 'nie pog��biona', 'negatywny', 'hipoteczny', '70000.0', '9', '2', '20.00', 'nie', 'tak');
INSERT INTO WNIOSKI VALUES ('2017-02-14 17:16:10', 'pog��biona', 'pozytywny', 'hipoteczny', '300000.0', '8', '3', '30.00', 'nie', 'nie');
INSERT INTO WNIOSKI VALUES ('2017-01-16 19:58:46', 'nie pog��biona', 'negatywny', 'hipoteczny', '500000.0', '7', '4', '10.00', 'tak', 'nie');
INSERT INTO WNIOSKI VALUES ('2017-01-16 08:00:00', 'pog��biona', 'wstrzymany', 'hipoteczny', '3000000.0', '6', '5', '50.00', 'tak', 'tak');
INSERT INTO WNIOSKI VALUES ('2017-03-06 09:00:31', 'pog��biona', 'negatywny', 'hipoteczny', '1000000.0', '5', '6', '10.00', 'nie', 'tak');
INSERT INTO WNIOSKI VALUES ('2017-03-06 12:00:31', 'nie pog��biona', 'pozytywny', 'hipoteczny', '300000.0', '4', '7', '70.00', 'tak', 'nie');
INSERT INTO WNIOSKI VALUES ('2017-03-07 12:00:31', 'pog��biona', 'wstrzymany', 'hipoteczny', '5000000.0', '3', '8', '10.00', 'nie', 'tak');
INSERT INTO WNIOSKI VALUES ('2017-03-08 12:10:31', 'nie pog��biona', 'pozytywny', 'hipoteczny', '500000.0', '2', '9', '40.00', 'tak', 'nie');
INSERT INTO WNIOSKI VALUES ('2017-03-08 10:10:31', 'nie pog��biona', 'wstrzymany', 'hipoteczny', '1000000.0', '1', '10', '50.00', 'tak', 'tak');

INSERT INTO WNIOSKI VALUES ('2017-03-08 11:10:31', 'pog��biona', 'pozytywny', 'hipoteczny', '2000000.0', '1', '10', '50.00', 'tak', 'tak');
INSERT INTO WNIOSKI VALUES ('2017-01-03 10:10:31', 'pog��biona', 'pozytywny', 'hipoteczny', '2000000.0', '1', '8', '50.00', 'tak', 'tak');
INSERT INTO WNIOSKI VALUES ('2017-01-03 11:10:31', 'nie pog��biona', 'negatywny', 'hipoteczny', '2000000.0', '1', '8', '50.00', 'tak', 'tak');
INSERT INTO WNIOSKI VALUES ('2017-01-03 11:40:31', 'nie pog��biona', 'pozytywny', 'hipoteczny', '2000000.0', '1', '8', '50.00', 'tak', 'tak');
INSERT INTO WNIOSKI VALUES ('2017-01-03 12:40:31', 'nie pog��biona', 'pozytywny', 'hipoteczny', '2000000.0', '1', '8', '50.00', 'tak', 'tak');


--wypelnienie ZGL_ORG_SCIG
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '1');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '2');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '3');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '4');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '5');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '6');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '7');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '8');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '9');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '10');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '11');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '12');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '13');
INSERT INTO ZGL_ORG_SCIG VALUES ('wy�udzenie pieni�dzy', 'policja', '14');