--Zestawienie wykonanych telefonow przez wszystkich pracownikow w dniu 03.012017r.
SELECT A.Imie, A.Nazwisko, WNIOSKI.Data_rozpatrzenia
FROM PRACOWNICY A
INNER JOIN WNIOSKI wnioski ON A.Id_Pracownika = wnioski.Id_pracownika
INNER JOIN ZGL_ORG_SCIG telefony ON wnioski.Id_wniosku = telefony.Id_Wniosku
WHERE YEAR(wnioski.Data_rozpatrzenia) = 2017 AND MONTH(wnioski.Data_rozpatrzenia) = 01 AND DAY(wnioski.Data_rozpatrzenia) = 03 