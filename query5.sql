--Pracownicy kt�rzy maja duzo pktow i wnioski rozpatrzone powyzej sredniej
SELECT pracownicy.Id_Pracownika,pracownicy.Imie, pracownicy.Nazwisko, SUM(czynnosci.Punktacja) punkty, AVG(WNIOSKI.Kwota)
FROM PRACOWNICY AS pracownicy
INNER JOIN AKTYWNOSCI aktywnosci ON pracownicy.Id_Pracownika = aktywnosci.Id_pracownika
INNER JOIN CZYNNOSCI czynnosci ON aktywnosci.Id_czynnosci = czynnosci.Id
INNER JOIN WNIOSKI wnioski ON wnioski.Id_pracownika = pracownicy.Id_Pracownika
WHERE (SELECT AVG(WNIOSKI.Kwota) FROM WNIOSKI) < wnioski.Kwota
GROUP BY pracownicy.Id_Pracownika, pracownicy.Imie, pracownicy.Nazwisko
HAVING SUM(czynnosci.Punktacja) > 10
ORDER BY 2 ASC