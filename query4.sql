--------Poka� okresowe zestawienie ilo�ci wykonanych telefon�w
--Pracownicy kt�rzy maja duzo pktow i wyplaty powy�ej �redniej

SELECT pracownicy.Id_Pracownika,pracownicy.Imie, pracownicy.Nazwisko, SUM(czynnosci.Punktacja) punkty
FROM PRACOWNICY AS pracownicy
INNER JOIN AKTYWNOSCI aktywnosci ON pracownicy.Id_Pracownika = aktywnosci.Id_pracownika
INNER JOIN WYPLATY wyplaty ON  pracownicy.Id_Pracownika = wyplaty.Id_pracownika 
INNER JOIN CZYNNOSCI czynnosci ON aktywnosci.Id_czynnosci = czynnosci.Id
WHERE (SELECT AVG(WYPLATY.Wyplata_podstawowa) FROM WYPLATY) > wyplaty.Wyplata_podstawowa
GROUP BY pracownicy.Id_Pracownika, pracownicy.Imie, pracownicy.Nazwisko
HAVING SUM(czynnosci.Punktacja) > 20