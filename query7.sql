--Pokaz ilosc pktow zdobytych przez poszczegolnych pracownikow w styczniu
SELECT pracownicy.Id_Pracownika,pracownicy.Imie, pracownicy.Nazwisko, SUM(czynnosci.Punktacja) punkty
FROM PRACOWNICY AS pracownicy
INNER JOIN AKTYWNOSCI aktywnosci ON pracownicy.Id_Pracownika = aktywnosci.Id_pracownika
INNER JOIN CZYNNOSCI czynnosci ON aktywnosci.Id_czynnosci = czynnosci.Id
WHERE MONTH(aktywnosci.Data_aktywnosci) = 01
GROUP BY pracownicy.Id_Pracownika,pracownicy.Imie, pracownicy.Nazwisko
