--Pokaz okresowe zestawienie wstrzymanych wnioskow
SELECT YEAR(wnioski.Data_rozpatrzenia) Rok, MONTH(wnioski.Data_rozpatrzenia) miesiac, COUNT(wnioski.Id_wniosku) liczba_wstrymanych
FROM WNIOSKI AS wnioski
WHERE wnioski.Decyzja = 'wstrzymany'
GROUP BY MONTH(WNIOSKI.Data_rozpatrzenia), YEAR(wnioski.Data_rozpatrzenia)