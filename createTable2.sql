--tabela czynnosci
CREATE TABLE CZYNNOSCI (
	Nazwa varchar(20) PRIMARY KEY,
	Punktacja int NOT NULL
);

--tabela PRACOWNICY
CREATE TABLE PRACOWNICY (
	Id_Pracownika int identity(1,1) PRIMARY KEY,
	Imie varchar(20) NOT NULL,
	Nazwisko varchar(20) NOT NULL,
	Stanowisko varchar(20) NOT NULL,
	PESEL varchar(11),
	Miejsce_zamieszkania varchar(50) NOT NULL,
	Miejsce_zameldowania varchar(50) NOT NULL,
	Nr_konta varchar(30) NOT NULL
);

CREATE TABLE KLIENCI (
	Id_Klienta int IDENTITY(1,1) PRIMARY KEY
);

--tabela WNIOSKI
CREATE TABLE WNIOSKI(
	Id_wniosku int identity(1,1) PRIMARY KEY,
	Rodzaj_weryfikacji varchar(14) NOT NULL,
	Decyzja varchar(10) NOT NULL,
	Kredyt varchar(20) NOT NULL,
	Kwota money NOT NULL,
	Id_klienta int REFERENCES KLIENCI,
	Id_pracownika int REFERENCES PRACOWNICY,
	Wsk_przeterminowania decimal(5,2) NOT NULL,
	Wsparcie_innej_komorki varchar(3) NOT NULL,
	Firma_niepotwierdzajaca varchar(3) NOT NULL
);


--tabela AKTYWNOSCI
CREATE TABLE AKTYWNOSCI (
	Id_aktywnosci int IDENTITY(1,1) PRIMARY KEY,
	Nazwa_czynnosci varchar(20) REFERENCES CZYNNOSCI,
	Data_aktywnosci datetime NOT NULL,
	Id_pracownika int REFERENCES PRACOWNICY
);


--tabela ABSENCJE
CREATE TABLE ABSENCJE (
	Id_absencji int IDENTITY(1,1) PRIMARY KEY,
	Data_poczatku datetime NOT NULL,
	Data_konca datetime NOT NULL,
	Typ_absencji varchar(20) NOT NULL,
	Id_pracownika int REFERENCES PRACOWNICY
);


--tabela WYPLATY
CREATE TABLE WYPLATY (
	Id_wyplaty int IDENTITY(1,1) PRIMARY KEY,
	Wyplata_podstawowa money NOT NULL,
	Data_wyplaty datetime NOT NULL,
	Id_pracownika int REFERENCES PRACOWNICY
);

--tabela ZG�_ORG_SCIG
CREATE TABLE ZGL_ORG_SCIG (
	Id_Zgloszenia int IDENTITY(1,1) PRIMARY KEY,
	Powod varchar(30) NOT NULL,
	Organ_scigania varchar(20) NOT NULL,
	Id_Wniosku int REFERENCES WNIOSKI
);

--tabela KLIENCI_INDYWIDUALNI
CREATE TABLE KLIENCI_INDYWIDALNI (
	Imie varchar(20) NOT NULL,
	Nazwisko varchar(20) NOT NULL,
	PESEL varchar(11),
	Miejsce_zamieszkania varchar(30) NOT NULL,
	Miejsce_zameldowania varchar(30) NOT NULL,
	Id_Klienta int REFERENCES KLIENCI
);

--tabela KLIENCI_FIRMY
CREATE TABLE KLIENCI_FIRMY (
	Nazwa_firmy varchar(20) NOT NULL,
	NIP varchar(14) NOT NULL,
	Siedziba varchar(20),
	Id_Klienta int REFERENCES KLIENCI
);

--tabela ZMIANY
CREATE TABLE ZMIANY (
	Id_zmiany int  IDENTITY(1,1) NOT NULL,
	Data_poczatku datetime NOT NULL,
	Data_konca datetime NOT NULL,
	Id_pracownika int REFERENCES PRACOWNICY
);

ALTER TABLE PRACOWNICY
ADD Opinia_przelozonego varchar(30) NULL