--tabela czynnosci
CREATE TABLE CZYNNOSCI (
	Id int identity(1,1) PRIMARY KEY,
	Nazwa varchar(20) NOT NULL,
	Punktacja int NOT NULL
);

--tabela PRACOWNICY
CREATE TABLE PRACOWNICY (
	Id_Pracownika int identity(1,1) PRIMARY KEY,
	Imie varchar(20) NOT NULL,
	Nazwisko varchar(20) NOT NULL,
	Stanowisko varchar(20) NOT NULL,
	PESEL varchar(11),
	Miejsce_zamieszkania varchar(30) NOT NULL,
	Miejsce_zameldowania varchar(30) NOT NULL,
	Nr_konta varchar(30) NOT NULL
);

CREATE TABLE ZMIANY (
	Id_zmiany int IDENTITY(1,1) PRIMARY KEY,
	Data_poczatku datetime NOT NULL,
	Data_koniec datetime NOT NULL,
	Id_pracownika int FOREIGN KEY REFERENCES PRACOWNICY
);

CREATE TABLE KLIENCI (
	Id_Klienta int PRIMARY KEY UNIQUE
);

--tabela WNIOSKI
CREATE TABLE WNIOSKI(
	Id_wniosku int identity(1,1) PRIMARY KEY,
	Data_rozpatrzenia datetime NOT NULL,
	Rodzaj_weryfikacji varchar(14) CHECK(Rodzaj_weryfikacji IN ('pog��biona', 'nie pog��biona')) NOT NULL, --zmieniono
	Decyzja varchar(10) CHECK(Decyzja IN ('pozytywny', 'negatywny', 'wstrzymany')) NOT NULL,	--zmieniono
	Kredyt varchar(20) NOT NULL,
	Kwota money NOT NULL,
	Id_klienta int FOREIGN KEY REFERENCES KLIENCI,
	Id_pracownika int FOREIGN KEY REFERENCES PRACOWNICY,
	Wsk_przeterminowania decimal(5,2) NOT NULL,
	Wsparcie_innej_komorki varchar(3) CHECK(Wsparcie_innej_komorki IN ('tak', 'nie')) NOT NULL,		--zmieniono
	Firma_niepotwierdzajaca varchar(3) CHECK(Firma_niepotwierdzajaca IN ('tak', 'nie')) NOT NULL	--zmieniono
);


--tabela AKTYWNOSCI
CREATE TABLE AKTYWNOSCI (
	Id_aktywnosci int IDENTITY(1,1) PRIMARY KEY,
	Id_czynnosci int FOREIGN KEY REFERENCES CZYNNOSCI ON DELETE CASCADE,
	Data_aktywnosci datetime NOT NULL,
	Id_pracownika int REFERENCES PRACOWNICY
);


--tabela ABSENCJE
CREATE TABLE ABSENCJE (
	Id_absencji int IDENTITY(1,1) PRIMARY KEY,
	Data_poczatku datetime NOT NULL,
	Data_konca datetime NOT NULL,
	Typ_absencji varchar(20) NOT NULL,
	Id_pracownika int FOREIGN KEY REFERENCES PRACOWNICY
);


--tabela WYPLATY
CREATE TABLE WYPLATY (
	Id_wyplaty int IDENTITY(1,1) PRIMARY KEY,
	Wyplata_podstawowa money NOT NULL,
	Data_wyplaty datetime NOT NULL,
	Id_pracownika int FOREIGN KEY REFERENCES PRACOWNICY
);

--tabela ZG�_ORG_SCIG
CREATE TABLE ZGL_ORG_SCIG (
	Id_Zgloszenia int IDENTITY(1,1) PRIMARY KEY,
	Powod varchar(40) NOT NULL,
	Organ_scigania varchar(20) NOT NULL,
	Id_Wniosku int FOREIGN KEY REFERENCES WNIOSKI
);

--tabela KLIENCI_INDYWIDUALNI
CREATE TABLE KLIENCI_INDYWIDALNI (
	Id_klienta int NOT NULL,
	Imie varchar(20) NOT NULL,
	Nazwisko varchar(20) NOT NULL,
	PESEL varchar(11),
	Miejsce_zamieszkania varchar(40) NOT NULL,
	Miejsce_zameldowania varchar(40) NOT NULL,
	Id_Klienta int FOREIGN KEY REFERENCES KLIENCI
);

--tabela KLIENCI_FIRMY
CREATE TABLE KLIENCI_FIRMY (
	Id_klienta int NOT NULL,
	Nazwa_firmy varchar(40) NOT NULL,
	NIP varchar(14) NOT NULL,
	Siedziba varchar(40),
	Id_Klienta int FOREIGN KEY REFERENCES KLIENCI
);

ALTER TABLE PRACOWNICY
ADD Opinia_przelozonego varchar(40) NULL