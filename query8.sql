--Pokazac ile poszczegolni pracownicy przepracowali godzin w styczniu 2017
SELECT pracownicy.Id_Pracownika,pracownicy.Imie, pracownicy.Nazwisko, SUM(DATEDIFF(SECOND, zmiany.Data_poczatku, zmiany.Data_koniec)/3600.0) punkty
FROM PRACOWNICY AS pracownicy
INNER JOIN ZMIANY zmiany ON  pracownicy.Id_Pracownika = zmiany.Id_pracownika
WHERE YEAR(zmiany.Data_poczatku) = 2017 AND MONTH(zmiany.Data_poczatku)= 01
GROUP BY pracownicy.Id_Pracownika,pracownicy.Imie, pracownicy.Nazwisko