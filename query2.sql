--Poka� okresowe zestawienie ilo�ci wniosk�w pozytywnych
SELECT MONTH(WNIOSKI.Data_rozpatrzenia)	AS miesiac,
		COUNT(WNIOSKI.Data_rozpatrzenia) AS ilosc_pozytywnych
FROM WNIOSKI AS wnioski
WHERE wnioski.Decyzja = 'pozytywny'
GROUP BY MONTH(WNIOSKI.Data_rozpatrzenia) 